var React = require('react');
var utils = require('../utils/utils');

var DayItem = React.createClass({

    render: function() {
        var date = utils.getDate(this.props.day.dt);
        var icon = this.props.day.weather[0].icon;
        var index = this.props.index;

        return (
            <div className="col-sm-4" role="button" onClick={this.props.onClick.bind(null, index)}>
                <img src={'./app/images/weather-icons/' + icon + '.svg'} alt="Weather" />
                <h2>{date}</h2>
            </div>
        );
    }

});

var ForecastsUi = React.createClass({

    render: function() {
        return (
            <div className="container">
                <div className="row">
                    {this.props.weatherInfo.map(function(weatherItem, i) {
                        return <DayItem key={weatherItem.dt} day={weatherItem} onClick={this.props.onClick} index={i} />
                    }.bind(this))}
                </div>
            </div>
        );
    }

});

var Forecasts = React.createClass({

    propTypes: {
        isLoading: React.PropTypes.bool.isRequired,
        weatherInfo: React.PropTypes.array.isRequired,
        location: React.PropTypes.string.isRequired,
        onDayClick: React.PropTypes.func.isRequired
    },

    render: function() {

        if (this.props.isLoading) {
            return (
                <h1 className="text-center">Loading data for {this.props.location}, please wait...</h1>
            );
        }

        return (
            <div>
                <h1 className="text-center">{this.props.location}</h1>
                <h2 className="text-center">select a day</h2>
                <ForecastsUi weatherInfo={this.props.weatherInfo} onClick={this.props.onDayClick} />
            </div>
        );
    }
});

module.exports = Forecasts;
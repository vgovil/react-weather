var React = require('react');

var GetWeather = React.createClass({

    propTypes: {
        position: React.PropTypes.string.isRequired,
        value: React.PropTypes.string.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onSubmit: React.PropTypes.func.isRequired
    },

    render: function() {
        return (
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form className={this.props.position} role="search">
                    <div className="form-group">
                      <input type="text" value={this.props.value} onChange={this.props.onUpdate} className="form-control" placeholder="Enter a city..." />
                    </div>
                    <button type="submit" className="btn btn-success" onClick={this.props.onSubmit}>Get Weather</button>
                </form>
            </div>
        );
    }

});

module.exports = GetWeather;
var React = require('react');
var WeatherContainer = require('../containers/WeatherContainer');

var Home = React.createClass({

    render: function() {
        return (
            <div>
                <h1 className="text-center">Enter City and State</h1>
                <WeatherContainer position="navbar-form text-center" />
            </div>
        );
    }

});

module.exports = Home;
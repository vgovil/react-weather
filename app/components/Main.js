var React = require('react');
var Header = require('../components/Header');

var Main = React.createClass({

    render: function() {
        return (
            <div className='main-container'>
                <Header/>
                <content>
                    {this.props.children}
                </content>
            </div>
        );
    }

});

module.exports = Main;
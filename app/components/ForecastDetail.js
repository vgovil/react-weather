var React = require('react');
var utils = require('../utils/utils');

var ForecastDetail = React.createClass({

    propTypes: {
        day: React.PropTypes.object.isRequired
    },

    render: function() {
        return (
            <div className="text-center">
                <p>{utils.getDate(this.props.day.dt)}</p>
                <p>{this.props.location}</p>
                <p>{this.props.day.weather[0].description}</p>
                <p>min temp: {utils.convertTemp(this.props.day.temp.min)} degrees</p>
                <p>max temp: {utils.convertTemp(this.props.day.temp.max)} degrees</p>
                <p>humidity: {this.props.day.humidity}</p>
            </div>
        );
    }

});

module.exports = ForecastDetail;
var React = require('react');
var WeatherContainer = require('../containers/WeatherContainer');

var Header = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    onHome: function() {
      this.context.router.push('');
    },

    render: function() {
        return (
            <nav className="navbar navbar-default">
              <div className="container-fluid">
                <div className="navbar-header">
                  <a className="navbar-brand" href="#" onClick={this.onHome}>Weather</a>
                </div>
                <WeatherContainer position="navbar-form navbar-right" />
              </div>
            </nav>
        );
    }
});

module.exports = Header;
var axios = require('axios');

var apiKey = "7142b611a97dabb4bcd63c251e4b591e";

var url = "http://api.openweathermap.org/data/2.5/forecast/daily?type=accurate&APPID=7142b611a97dabb4bcd63c251e4b591e&cnt=5&q=";

var apiHelper = {

    getWeather: function(location) {
        return axios.get(url + location).then(function(information) {
            return information.data;
        }).catch(function(error){
            console.error('Error occured while fetching weather ' + error);
        });
    }

};

module.exports = apiHelper;
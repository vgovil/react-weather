var React = require('react');
var Forecasts = require('../components/Forecasts');
var apiHelper = require('../utils/apiHelper');

var ForecastContainer = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    getInitialState: function() {
        return {
            isLoading: true,
            weatherInfo: []
        };
    },

    componentDidMount: function() {
        apiHelper.getWeather(this.props.routeParams.location).then(function(weatherInformation) {
            this.setState({
                isLoading: false,
                weatherInfo: weatherInformation.list
            });
        }.bind(this));
    },

    handleDayClick: function(index, e) {
        var selectedDay = this.state.weatherInfo[index];
        this.context.router.push({
            pathname: '/detail/' + this.props.routeParams.location,
            state: {
                dayWeather: selectedDay
            }
        })
    },

    render: function() {
        return (
            <Forecasts
                location={this.props.routeParams.location}
                isLoading={this.state.isLoading}
                weatherInfo={this.state.weatherInfo}
                onDayClick={this.handleDayClick}
            />
        );
    }

});

module.exports = ForecastContainer;
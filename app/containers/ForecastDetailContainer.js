var React = require('react');
var ForecastDetail = require('../components/ForecastDetail');

var ForecastDetailContainer = React.createClass({

    render: function() {
        return (
            <ForecastDetail day={this.props.location.state.dayWeather} location={this.props.routeParams.location} />
        );
    }

});

module.exports = ForecastDetailContainer;
var React = require('react');
var GetWeather = require('../components/GetWeather');

/**
 * Handles events as well as rendering of search
 */
var WeatherContainer = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    propTypes: {
        position: React.PropTypes.string.isRequired
    },

    getInitialState: function() {
        return {
            location: ''
        };
    },

    updateLocation: function(e) {
        this.setState({
            location: e.target.value
        })
    },

    searchWeather: function(e) {
        e.preventDefault();
        this.context.router.push('/forecast/' + this.state.location);
    },

    render: function() {
        return (
            <GetWeather
                position={this.props.position}
                onUpdate={this.updateLocation}
                value={this.state.location}
                onSubmit={this.searchWeather}
            />
        );
    }

});

module.exports = WeatherContainer;
var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var hashHistory = ReactRouter.hashHistory;
var Main = require('../components/Main');
var Home = require('../components/Home');
var ForecastContainer = require('../containers/ForecastContainer');
var ForecastDetailContainer = require('../containers/ForecastDetailContainer');

var routes = (
    <Router history={hashHistory}>
        <Route path='/' component={Main}>
            <IndexRoute component={Home}></IndexRoute>
            <Route path='forecast/:location' component={ForecastContainer}/>
            <Route path='detail/:location' component={ForecastDetailContainer}/>
        </Route>
    </Router>
);

module.exports = routes;
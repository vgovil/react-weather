# README #

A weather app based on React (tutorial from [ReactJS program](https://github.com/ReactjsProgram/react-fundamentals-curriculum))

### What is this repository for? ###

* Learning react, components, react-router, axios
* Version 1.0.0

### How do I get set up? ###

* Clone this repository locally
* Open command prompt, navigate to the root folder of this project
* run `npm install`
* run `npm install --only=dev`
* run `node start`
* Open the url (http://localhost:8080) in the browser